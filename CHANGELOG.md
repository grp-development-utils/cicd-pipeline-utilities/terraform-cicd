Changelog
[1.2.1] - 2025-01-15
### Changed
- consumer/components/terraform-deploy.yml: Added project name to terraform deploy component

[1.2.0] - 2025-01-15
### Changed
- consumer/components/terraform-deploy.yml: Added default tags to terraform deploy component

[1.1.0] - 2025-01-05
### Changed
- consumer/components/terraform-deploy.yml:
  - Added backend configuration for state storage in S3.
  - Added Support for tfvars
- consumer/deployment-template.yml: Added a flag to enable disable the format file structure job

[1.0.0] - 2025-01-05
### Added
- CHANGELOG.md: Added file to track changes in the project.
### Changed
- consumer/components/terraform-deploy.yml: Updated docker image to 1.10.3
