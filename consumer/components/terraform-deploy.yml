variables:
  TERRAFORM_IMAGE_VERSION: "1.10.4" # Default Terraform image version, can be overridden
  TF_ROOT: ${CI_PROJECT_DIR}
  PLAN: ${CI_PROJECT_NAME}-${CI_COMMIT_BRANCH}.tfplan
  STATE: ${CI_PROJECT_NAME}-${CI_COMMIT_BRANCH}.tfstate

  # Terraform variables
  TF_VAR_project_name: $CI_PROJECT_NAME
  TF_VAR_branch: $CI_COMMIT_REF_NAME
  TF_VAR_default_tags: |
    {
      "ProjectName": "$CI_PROJECT_NAME",
      "GitLabIdentifier": "$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME/$CI_COMMIT_REF_NAME"
      "ManagedBy": "Terraform",
      "CreatedBy": "GitLabCI",
      "PipelineID": "$CI_PIPELINE_ID"
    }

  #custom variables
  SEARCH_DIR: modules/

  # Environment-specific variables
  TF_ENV_DIR: "tf_env_files" # Directory for environment-specific files
  TFVARS_FILE: "$TF_ENV_DIR/dev.tfvars"

  #S3 State Files Bucket Configuration
  S3_BUCKET: "ugf.bucket.tf.state.files"
  REGION: "us-west-1"
  BACKEND_KEY: "terraform_state_files/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}/branches/${CI_COMMIT_REF_NAME}.tfstate" # Dynamic key based on branch or tag

default:
  image:
    name: "hashicorp/terraform:${TERRAFORM_IMAGE_VERSION}"
    entrypoint:
      - '/usr/bin/env'
      - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
  before_script:
    - terraform --version
    - echo "Using Terraform variables from $TFVARS_FILE"

init:
  stage: terraform_init
  needs: []
  script:
    - echo "Initializing Terraform"
    - terraform init -backend-config="bucket=$S3_BUCKET" -backend-config="key=$BACKEND_KEY" -backend-config="region=$REGION" -backend-config="encrypt=true"
  artifacts:
    paths:
      - .terraform
      - .terraform.lock.hcl

format:
  stage: terraform_validation
  needs: [init]
  script:
  - terraform fmt -check -diff -recursive

validate:
  stage: terraform_validation
  needs: [init]
  script:
    - terraform validate

plan:
  stage: terraform_plan
  needs: [init, validate]
  script:
  - |
    if [ -n "$CI_COMMIT_TAG" ]; then
      # If it's a tagged commit, use prod.tfvars
      export TFVARS_FILE="$TF_ENV_DIR/prod.tfvars"
      export BACKEND_KEY="terraform_state_files/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}/tags/${CI_COMMIT_TAG}.tfstate"
    elif [ "$CI_COMMIT_REF_NAME" == "main" ] || [ "$CI_COMMIT_REF_NAME" == "master" ]; then
      # If it's the main or master branch, use test.tfvars
      export TFVARS_FILE="$TF_ENV_DIR/test.tfvars"
    else
      # For all other branches, use dev.tfvars
      export TFVARS_FILE="$TF_ENV_DIR/dev.tfvars"
    fi

    # Now run terraform plan using the appropriate TFVARS_FILE
    terraform plan -var-file=$TFVARS_FILE -state=$STATE -out=$PLAN
  artifacts:
    name: plan_logs
    paths:
      - $PLAN
      - plan_output.log
      - $STATE

apply:
  when: manual
  stage: create_resources
  needs: [init, plan]
  allow_failure: false
  script:
  - |
    if [ -n "$CI_COMMIT_TAG" ]; then
      # If it's a tagged commit, use prod.tfvars
      export TFVARS_FILE="$TF_ENV_DIR/prod.tfvars"
    elif [ "$CI_COMMIT_REF_NAME" == "main" ] || [ "$CI_COMMIT_REF_NAME" == "master" ]; then
      # If it's the main or master branch, use test.tfvars
      export TFVARS_FILE="$TF_ENV_DIR/test.tfvars"
    else
      # For all other branches, use dev.tfvars
      export TFVARS_FILE="$TF_ENV_DIR/dev.tfvars"
    fi

    # Now run terraform plan using the appropriate TFVARS_FILE
    terraform apply -var-file=$TFVARS_FILE -state=$STATE -input=false $PLAN > apply_output.log 2>&1
  after_script:
    # Save Terraform outputs as JSON to a file
    - terraform output -json > terraform_outputs.json
  artifacts:
    name: apply_logs
    paths:
      - terraform_outputs.json
      - apply_output.log
      - $STATE

destroy:
  stage: destroy_resources
  when: manual
  script:
  - |
    if [ -n "$CI_COMMIT_TAG" ]; then
      # If it's a tagged commit, use prod.tfvars
      export TFVARS_FILE="$TF_ENV_DIR/prod.tfvars"
    elif [ "$CI_COMMIT_REF_NAME" == "main" ] || [ "$CI_COMMIT_REF_NAME" == "master" ]; then
      # If it's the main or master branch, use test.tfvars
      export TFVARS_FILE="$TF_ENV_DIR/test.tfvars"
    else
      # For all other branches, use dev.tfvars
      export TFVARS_FILE="$TF_ENV_DIR/dev.tfvars"
    fi

    # Now run terraform plan using the appropriate TFVARS_FILE
    terraform destroy -var-file=$TFVARS_FILE -state=$STATE -auto-approve > destroy_output.log 2>&1
  artifacts:
    name: destroy_logs
    paths:
      - destroy_output.log
      - $STATE
